//Construimos un modelo. Aqui construimos lo que nos tiene que traer la api a de ser similar a nuestro json

export interface Books {
  id: number;
  Titulo: string;
  Autor: string;
  year: string;
  Saga: string;
  Categoria: string;
  image: Image;
}

export interface Detalles {
  id: number;
  Argumento: string;
  Puntuacion: number;
  image: string;
}

export interface Image {
  url: string;
  alt: string;
}
export interface Votos {
  addlike: number;
  adddislike: number;
}
