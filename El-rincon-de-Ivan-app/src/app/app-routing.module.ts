import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () =>
      import('./Pages/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'books',
    loadChildren: () =>
      import('./Pages/books/books.module').then((m) => m.BooksModule),
  },
  {
    path: 'about',
    loadChildren: () =>
      import('./Pages/about/about.module').then((m) => m.AboutModule),
  },
  {
    path: 'detail/:id',
    loadChildren: () =>
      import('./Pages/detail/detail.module').then((m) => m.DetailModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
