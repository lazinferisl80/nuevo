import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  public imageLogo: string =
    'https://i.pinimg.com/236x/37/5a/12/375a123334b6c258501832b855097739--my-feelings-letter-i.jpg';
  public altLogo: string = 'letra i de fuego';

  constructor() {}

  ngOnInit(): void {}
}
