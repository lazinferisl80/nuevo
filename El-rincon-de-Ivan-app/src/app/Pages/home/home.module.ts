import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home-component/home.component';
import { HomeTituloComponent } from './home-component/home-titulo/home-titulo.component';
import { HomeDefinicionComponent } from './home-component/home-definicion/home-definicion.component';

@NgModule({
  declarations: [HomeComponent, HomeTituloComponent, HomeDefinicionComponent],
  imports: [CommonModule, HomeRoutingModule],
})
export class HomeModule {}
