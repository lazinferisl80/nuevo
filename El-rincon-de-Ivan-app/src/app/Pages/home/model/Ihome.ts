export interface Titulo {
  image: string;
  titulo: string;
  subtitulo: string;
  contenido: string;
}
