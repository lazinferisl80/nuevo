import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeDefinicionComponent } from './home-definicion.component';

describe('HomeDefinicionComponent', () => {
  let component: HomeDefinicionComponent;
  let fixture: ComponentFixture<HomeDefinicionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeDefinicionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeDefinicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
