import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeTituloComponent } from './home-titulo.component';

describe('HomeTituloComponent', () => {
  let component: HomeTituloComponent;
  let fixture: ComponentFixture<HomeTituloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeTituloComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeTituloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
