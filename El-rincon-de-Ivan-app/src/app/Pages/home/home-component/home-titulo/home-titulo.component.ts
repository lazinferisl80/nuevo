import { Component, OnInit, Input } from '@angular/core';
import { Titulo } from '../../model/Ihome';

@Component({
  selector: 'app-home-titulo',
  templateUrl: './home-titulo.component.html',
  styleUrls: ['./home-titulo.component.css'],
})
export class HomeTituloComponent implements OnInit {
  @Input()
  titulo!: Titulo;

  constructor() {}

  ngOnInit(): void {}
}
