import { Component, OnInit } from '@angular/core';
import { Titulo } from '../model/Ihome';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  public titulo: Titulo = {
    image:
      'https://i.pinimg.com/236x/37/5a/12/375a123334b6c258501832b855097739--my-feelings-letter-i.jpg',
    titulo: 'Mi titulo',
    subtitulo: 'Mira lo que se avecina',
    contenido: 'Mira lo que se avecina por la vuelta de la esquina',
  };
  constructor() {}

  ngOnInit(): void {}
}
