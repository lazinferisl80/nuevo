import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemBooksComponent } from './item-books.component';

describe('ItemBooksComponent', () => {
  let component: ItemBooksComponent;
  let fixture: ComponentFixture<ItemBooksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemBooksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemBooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
