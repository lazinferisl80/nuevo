import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Books, Votos, Detalles } from 'src/app/Models/models';

@Component({
  selector: 'app-item-books',
  templateUrl: './item-books.component.html',
  styleUrls: ['./item-books.component.css'],
})
export class ItemBooksComponent implements OnInit {
  @Input()
  item!: Books;
  @Input() detail!: Detalles;

  @Output() addLike = new EventEmitter<number>();
  @Output() addDisLike = new EventEmitter<number>();

  addlike: Votos | any = {};
  adddislike: Votos | any = {};

  //input recibe
  //output envian

  constructor() {}

  ngOnInit(): void {}

  /*respuesta = 0;

  setLike() {
    this.respuesta = +1;
  }

  setDisLike() {
    this.respuesta = +1;
  }*/

  //el evento ha de ser escuchado por el padre que en este caso es bookscomponent
}
