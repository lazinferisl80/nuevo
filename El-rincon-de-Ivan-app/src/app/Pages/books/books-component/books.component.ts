import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { RinconService } from 'src/app/Services/rinconService/rincon.service';
import { Books } from 'src/app/Models/models';
import { Location } from '@angular/common';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css'],
})
export class BooksComponent implements OnInit {
  @Output() addLike = new EventEmitter<number>();
  @Output() addDisLike = new EventEmitter<number>();
  public itemBooks!: Books[];

  constructor(
    private rinconService: RinconService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getList();
  }

  public getList(): void {
    this.rinconService.getList().subscribe(
      (data: any[]) => {
        this.itemBooks = data;
      },
      (err) => {
        console.error(err.message);
      }
    );
  }
  public goBack(): void {
    this.location.back();
  }
}
//getItemBooks
