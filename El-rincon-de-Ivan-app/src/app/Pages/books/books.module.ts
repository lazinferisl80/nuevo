import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BooksRoutingModule } from './books-routing.module';
import { BooksComponent } from './books-component/books.component';
import { ItemBooksComponent } from './books-component/item-books/item-books.component';
//import { DetailComponent } from './books-component/detail/detail-component/detail.component';

@NgModule({
  declarations: [BooksComponent, ItemBooksComponent],
  imports: [CommonModule, BooksRoutingModule],
})
export class BooksModule {}
