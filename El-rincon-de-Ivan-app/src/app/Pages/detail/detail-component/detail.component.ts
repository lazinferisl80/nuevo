import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { RinconService } from '../../../Services/rinconService/rincon.service';
import { Detalles } from '../../../Models/models';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
})
export class DetailComponent implements OnInit {
  public detail: Detalles | any = {};
  public itemId!: string | null;
  constructor(
    private rinconService: RinconService,
    private location: Location,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getDetail();
  }

  public getDetail(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      this.itemId = params.get('id');
    });
    this.rinconService.getDetail(Number(this.itemId)).subscribe(
      (data: Detalles) => {
        this.detail = data;
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

  public goBack(): void {
    this.location.back();
  }
}
