import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Url } from '../../Key/urls';
import { Books, Detalles } from '../../Models/models';

@Injectable({
  providedIn: 'root',
})
export class RinconService {
  private booksUrl = Url.urlBooks;
  private detailUrl = Url.urlDetalles;

  constructor(private http: HttpClient) {
    /*Empty*/
  }

  public getList(): Observable<Books[]> {
    return this.http.get(this.booksUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
  public getDetail(id: number): Observable<Detalles> {
    return this.http.get(`${this.detailUrl}/${id}`).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
          return response;
        }
      })
    );
  }
}
