import { TestBed } from '@angular/core/testing';

import { RinconService } from './rincon.service';

describe('RinconService', () => {
  let service: RinconService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RinconService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
